import org.junit.After;
import org.junit.Before;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;

public class FrameworkSetup {

    protected WebDriver driver;

    @Before
    public void Setup(){
        System.setProperty("webdriver.gecko.driver", "/home/marco/Documents/drivers/geckodriver");
        this.driver = new FirefoxDriver();
        this.driver.get("http://automationpractice.com/index.php");
    }

    @After
    public void CloseUp(){
        this.driver.close();
    }
}
